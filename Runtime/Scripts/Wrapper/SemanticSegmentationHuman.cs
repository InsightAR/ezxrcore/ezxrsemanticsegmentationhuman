using System.Runtime.InteropServices;
using BaseDetector = System.IntPtr;
using BaseTracker = System.IntPtr;

namespace EZXRCoreExtensions.SemanticSegmentationHuman
{
    [StructLayout(LayoutKind.Sequential)]
    public struct HumanDetTrackInfo
    {
        public float x1;
        public float y1;
        public float width;
        public float height;
        public int curr_id;
        public int label;
        public float prob;
    }
    
    public static partial class ExternApi
    {

#if (UNITY_IOS || UNITY_TVOS || UNITY_WEBGL) && !UNITY_EDITOR
        public const string LIBRARY_NAME = "__Internal";
#else
        public const string LIBRARY_NAME = "semanticsegmentationhuman";
#endif
        [DllImport(LIBRARY_NAME)]
        public static extern BaseDetector createDetector(string assetsDir);

        [DllImport(LIBRARY_NAME)]
        public static extern int detect(BaseDetector detector, IASInputImage inputImage, ref HumanDetTrackInfo humanDetTrackInfo);

        [DllImport(LIBRARY_NAME)]
        public static extern void destroyDetector(BaseDetector detector);

        [DllImport(LIBRARY_NAME)]
        public static extern BaseTracker createTracker();

        [DllImport(LIBRARY_NAME)]
        public static extern int tracking(BaseTracker tracker, ref HumanDetTrackInfo humanDetTrackInfo, int numPerson);

        [DllImport(LIBRARY_NAME)]
        public static extern void destroyTracker(BaseTracker tracker);
    }
    
}

