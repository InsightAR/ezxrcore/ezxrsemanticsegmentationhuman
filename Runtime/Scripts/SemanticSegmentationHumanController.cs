using System.Collections.Generic;
using UnityEngine;

using BaseDetector = System.IntPtr;
using BaseTracker = System.IntPtr;

namespace EZXRCoreExtensions.SemanticSegmentationHuman
{
    public class SemanticSegmentationHumanController
    {
        private string m_asset_path = "";
        private BaseDetector m_detector;
        private BaseTracker m_tracker; 

        private const int MAX_HUMAN_NUM = 10;
        private HumanDetTrackInfo[] m_humanDetTrackInfos = new HumanDetTrackInfo[MAX_HUMAN_NUM];

        /// <summary>
        /// 初始化
        /// </summary>
        /// <param name="model_path">模型路径</param>
        /// <returns></returns>
        public int StartDecetion(string model_path)
        {
            if (m_detector != System.IntPtr.Zero)
            {
                Debug.LogError("detector is created");
                return 0;
            }
            m_asset_path = model_path;
            Debug.Log("model_path:" + model_path);
            m_detector = ExternApi.createDetector(model_path);
            if (m_detector == System.IntPtr.Zero)
            {
                Debug.LogError("detector ctrate failed");
                return -1;
            }
            return 1;
        }
        
        /// <summary>
        /// 释放
        /// </summary>
        public void StopDecetion()
        {
            if (m_detector != System.IntPtr.Zero)
            {
                ExternApi.destroyDetector(m_detector);
                m_detector = System.IntPtr.Zero;
            }
        }

        /// <summary>
        /// 开始追踪
        /// </summary>
        /// <returns></returns>
        public int StartTracking()
        {
            if (m_tracker != System.IntPtr.Zero)
            {
                Debug.LogError("tracker is created");
                return 0;
            }
            if (m_detector == System.IntPtr.Zero)
            {
                Debug.LogError("detector is null");
                return -1;
            }
            m_tracker = ExternApi.createTracker();
            if (m_tracker == System.IntPtr.Zero)
            {
                Debug.LogError("tracker ctrate failed");
                return -2;
            }
            return 1;
        }

        /// <summary>
        /// 停止追踪
        /// </summary>
        public void StopTracking()
        {
            if (m_tracker != System.IntPtr.Zero)
            {
                ExternApi.destroyTracker(m_tracker);
                m_tracker = System.IntPtr.Zero;
            }
        }

        /// <summary>
        /// 检测
        /// </summary>
        /// <param name="inputImage">输入图像</param>
        /// <returns></returns>
        public int RunDetect(IASInputImage inputImage)
        {
            if (m_detector == System.IntPtr.Zero)
            {
                Debug.LogError("detector is null");
                return 0;
            }
            int human_num = ExternApi.detect(m_detector, inputImage, ref m_humanDetTrackInfos[0]);
            return human_num;
        }

        /// <summary>
        /// 追踪
        /// </summary>
        /// <param name="human_num">检测到的人数</param>
        /// <returns></returns>
        public List<HumanDetTrackInfo> RunTrack(int human_num)
        {
            int human_num_track = 0;
            if (m_tracker != System.IntPtr.Zero)
            {
                human_num_track = ExternApi.tracking(m_tracker, ref m_humanDetTrackInfos[0], human_num);
            }
            List<HumanDetTrackInfo> humanList = new List<HumanDetTrackInfo>();
            for (int i = 0; i < human_num_track; i++)
            {
                humanList.Add(m_humanDetTrackInfos[i]);
            }
            return humanList;
        }
    }
}