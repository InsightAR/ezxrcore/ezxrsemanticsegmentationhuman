# EZXR Semantic Segmentation Human

EZXR Semantic Segmentation Human 易现人体2d bounds检测跟踪。

## 对应版本

| Unity Version | EZXR Semantic Segmentation Human |
| ------------- | ----------- |
| 2022.3        | 0.1.1       |

## Release Note

### V0.1.1

1. 支持Device Orientation 为横屏的人体检测
2. 优化人体3d bounds的估算的位置精度和跳动优化
3. 增加config配置项"max_person_detection"、"max_detect_distance"、"score_thresh"
4. 增加人体2d bounds闪动优化策略
5. sample中的内存优化

### V0.1.0

1. 提供iOS/android EZXR Semantic Segmentation Human 易现人体2d bounds检测跟踪 算法库
2. 提供C#调用算法接口SemanticSegmentationHumanController类



## API 说明

```C#
/// <summary>
/// 初始化
/// </summary>
/// <param name="model_path">模型路径</param>
/// <returns></returns>
public int StartDecetion(string model_path)

/// <summary>
/// 释放
/// </summary>
public void StopDecetion()

/// <summary>
/// 开始追踪
/// </summary>
/// <returns></returns>
public int StartTracking()

/// <summary>
/// 停止追踪
/// </summary>
public void StopTracking()

/// <summary>
/// 检测
/// </summary>
/// <param name="inputImage">输入图像</param>
/// <returns></returns>
public int RunDetect(IASInputImage inputImage)

/// <summary>
/// 追踪
/// </summary>
/// <param name="human_num">检测到的人数</param>
/// <returns></returns>
public List<HumanDetTrackInfo> RunTrack(int human_num)
```

